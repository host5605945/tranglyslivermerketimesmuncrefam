// https://nuxt.com/docs/api/configuration/nuxt-config
import vuetify, {transformAssetUrls} from "vite-plugin-vuetify";
export default defineNuxtConfig({
  ssr:true,
  devtools: { 
    enabled: true ,
  },
  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxt/image",
    (_options, nuxt) => {
      nuxt.hooks.hook("vite:extendConfig", (config) => {
        // @ts-expect-error
        config.plugins.push(vuetify({autoImport: true}));
      });
    },
  ],
  tailwindcss: {
    cssPath: ["~/assets/css/tailwind.css", {injectPosition: "first"}],
    configPath: "tailwind.config",
    exposeConfig: {
      level: 2,
    },
    config: {},
    viewer: true,
  },
  runtimeConfig: {
    username: "",
    password: "",
    backendPassword: "xxxxxx-xxxx-xxxx-xxx-xxxx-xxxxxx",
    public: {
      bypassList: ["/"],
      paramDataMode:false,
      apiBase:undefined,
      staticMode:false,
      alwayMakeImageToWebp:false
    },
  },
  build: {
    transpile: ["vuetify"],
  },
  vite: {
    vue: {
      template: {
        transformAssetUrls,
      },
    },
    define: {global: "window"},
  },
  routeRules: {
  },
  nitro:{
    experimental:{
      openAPI:true
    },
  },
  
});
